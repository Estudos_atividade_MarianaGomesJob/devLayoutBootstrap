Olá!

Sou Mariana Gomes este repositório visa manter amazenado estudos de novas
tecnologias de acordo com as tendências do mercado. Tenha em mente que nem todos os códigos 
podem funcionar nem são padronizados de acordo com os padrões atuais de software que existem hoje. 
Sinta-se à vontade para revisar os códigos e aprender com eles.

Sumário

HTML 		- Experiências serão focadas em novas APIs HTML5.
CSS 		- Experimentos serão focados em Animações CSS3.
JavaScript 	- Os experimentos serão focados na linguagem e seus conceitos.

GPL-3.0 © Mariana Gomes